import java.security.SecureRandom;
import java.util.*;
public class Owner 
{
	private String OwnerName;
	private String OwnerId;
	private ArrayList<vehicles> cars;
	


 		public Owner(String Owner_Name, ArrayList<vehicles> OwnerCars)			// owner constructor
 		{
 			Owner_Name=OwnerName;
 			
 			OwnerCars=cars;
 			OwnerId=IDgenerator();
 		}
 		
 		public Owner()			// owner constructor
 		{
 			OwnerId=IDgenerator();
 			cars = new ArrayList<vehicles>();
 		}

 		public String IDgenerator()												//12 digit alphanumeric ID generator
 		{
 			
 			String start="";
 			int count=0;
 			String hold="";
 			SecureRandom rand=new SecureRandom();
 			char alphabet[]= 
 				{'A','B','C','D', 'E', 'F', 'G', 
                 'H', 'I', 'J', 'K', 'L', 'M', 'N',  
                 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 
                  'V', 'W', 'X', 'Y', 'Z'
 				};
 			
 			do {
 				for(int i=0;i<3;i++) 
 				{
 					hold=String.valueOf(rand.nextInt(999                                                                                                                         ));
 					start=start + alphabet[rand.nextInt(26)];
 					if(hold.length()==2) {
 						hold="0"+hold;
 					}
 					else if(hold.length()==1) {
 						hold="00"+hold;
 					}
 				}
 					start=start+hold;
					count++;
 			}while(count<2);
 		return start;	
 			
 		}



		public String getOwnerName() {										//generated setters and getters
			return OwnerName;
		}



		public ArrayList<vehicles> getCars() {
			return cars;
		}
		
		public void addCar(vehicles var){ 
			cars.add(var);
		}




 		public String getOwnerId() 
 		{
 			return OwnerId;
 		}

		public void setOwnerName(String ownerName) {
			OwnerName = ownerName;
		}

		public void setCars(ArrayList<vehicles> cars) {
			this.cars = cars;
		}



 	
		
}
