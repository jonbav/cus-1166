import java.security.SecureRandom;

public class clients 
{
	private String ClientId;
	//private Jobduration;
	private String Jobdeadline;
	
	public clients(String deadline) 
	{
		ClientId=IDgenerator();
		deadline=Jobdeadline;	
	}
	

	public String IDgenerator()												//12 digit alphanumeric ID generator
		{
			
			String start="";
			int count=0;
			String hold="";
			SecureRandom rand=new SecureRandom();
			char alphabet[]= 
				{'A','B','C','D', 'E', 'F', 'G', 
             'H', 'I', 'J', 'K', 'L', 'M', 'N',  
             'O', 'P', 'Q', 'R', 'S', 'T', 'U', 
              'V', 'W', 'X', 'Y', 'Z'
				};
			
			do {
				for(int i=0;i<3;i++) 
				{
					hold=String.valueOf(rand.nextInt(999                                                                                                                         ));
					start=start + alphabet[rand.nextInt(26)];
					if(hold.length()==2) {
						hold="0"+hold;
					}
					else if(hold.length()==1) {
						hold="00"+hold;
					}
				}
					start=start+hold;
				count++;
			}while(count<2);
		return start;	
			
		}


	public String getClientId() {
		return ClientId;
	}


	public String getJobdeadline() {
		return Jobdeadline;
	}

}
