import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.PriorityQueue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class GUITest {
	
	static VCC vcc;
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		vcc= new VCC();
	}
	
	
	
	
   // Tests if there is something in the Hashmap.
    
	@Test
	void test() {
		
		
		HashMap<String,Job> test = vcc.getJobs();
		
		Job job=new Job("XX",12345);
		
		
		vcc.registerJob(job);
		
		assertFalse(test.isEmpty());
		
	
	}
	
	
	 // Tests if the correct key is used to store the job.
	@Test
	void test1() {
		
		
		HashMap<String,Job> test = vcc.getJobs();
		
		Job job=new Job("XX",12345);
		
		
		vcc.registerJob(job);
		
		//assertEquals(job.getJobId(), vcc.getJobs().values());
		
		assertTrue(vcc.getJobs().containsKey(job.getJobId()));
		
	
	
	}
	
	// test to see if the correct job is stored, retains the same values.
	@Test
	void test2() {
		
		
	PriorityQueue<Job> testqueue = vcc.getJobQueue();
		
		Job job=new Job("XX",12345);
		Job testqueue1 = testqueue.poll();
		
		vcc.registerJob(job);
		
	
		
		assertSame(job.getJobId(),testqueue1.getJobId());
		

	
	}
	
	@Test
	void test3() throws IOException {
		
		vcc.PopulateVehicles();
	
		assertFalse(vcc.getVehicles().isEmpty());

	
	}
	// Testing to see if multiple vehicles are added to the hash map
	@Test
	void test4() throws IOException {
		
		vcc.PopulateVehicles();
	
		assertTrue(vcc.getVehicles().containsKey("3526526"));

	
	}
	
	
	@Test
	void test5() throws IOException {
		
		vcc.PopulateVehicles();
	
		assertTrue(vcc.getVehicles().containsKey("124590130592834065"));

	
	}
	
	@Test
	void test6()  {
		
		
		vcc.getJobQueue().clear();
		Job job=new Job("XX",12345);
		
		vcc.registerJob(job);
		
		assertEquals(vcc.getCompletionTime(job.getJobId()), 12345);
		

	
	}
	

	@Test
	void test7()  {
		
		
		vcc.getJobQueue().clear();
		Job job=new Job("XX",12345);
		Job job1=new Job("qX",12345);
		Job job2=new Job("XeX",12345);
		Job job3=new Job("XeXe",12345);
		vcc.registerJob(job);
		vcc.registerJob(job1);
		vcc.registerJob(job2);
		vcc.registerJob(job3);
		assertEquals(vcc.getCompletionTime(job2.getJobId()), 37035);
		

	
	}
}
